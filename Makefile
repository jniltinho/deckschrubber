BUILDTIME=$(shell date +'%Y%m%d')
#GIT_REV=$(shell git rev-parse HEAD|cut -c1-8)
LDFLAGS = -ldflags "-s -w -X main.buildTime=$(BUILDTIME) -X main.gitVersion=$(CI_COMMIT_SHORT_SHA)"

.PHONY: all

all: build-linux small-linux


build-linux:
	@echo "Build App Linux"
	GOOS=linux go build $(LDFLAGS) -v -o dist/deckschrubber
	ls -sh dist/


deps:
	go get github.com/docker/distribution
	go get github.com/docker/distribution/context
	go get github.com/docker/distribution/manifest/schema2
	go get github.com/docker/distribution/reference
	go get github.com/docker/distribution/registry/client
	go get github.com/sirupsen/logrus
	go get golang.org/x/crypto/ssh/terminal
	go get github.com/opencontainers/go-digest
	go get github.com/kardianos/govendor


small-linux:
	upx --ultra-brute dist/deckschrubber
	ls -sh dist/

install-govendor: deps
	# https://github.com/kardianos/govendor
	# docker run --rm -it -v "${PWD}:/go/src/gitlab.com/jniltinho/deckschrubber" golang:1.10 /bin/bash
	# cd /go/src/gitlab.com/jniltinho/deckschrubber
	rm -rf vendor
	govendor init
	govendor list
	govendor add +external
	du -h vendor/


install-upx:
	curl -skLO https://github.com/upx/upx/releases/download/v3.95/upx-3.95-amd64_linux.tar.xz
	tar -xf upx-3.95-amd64_linux.tar.xz
	cp upx-3.95-amd64_linux/upx /usr/local/bin/
	rm -rf upx-3.95-amd64_linux upx-3.95-amd64_linux.tar.xz

.PHONY: install-golang

install-golang: 
	wget https://dl.google.com/go/go1.10.4.linux-amd64.tar.gz
	tar -xvzf go1.10.4.linux-amd64.tar.gz -C /usr/local/
	rm -f go1.10.4.linux-amd64.tar.gz
	@echo "export PATH=\$$PATH:/usr/local/go/bin\nexport GOPATH=\$$HOME/go-workspace\nexport PATH=\$$PATH:$$GOPATH/bin" >> /etc/profile
	mkdir -p $(HOME)/go-workspace
