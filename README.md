# Deckschrubber

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/jniltinho/deckschrubber)](https://goreportcard.com/report/gitlab.com/jniltinho/deckschrubber)
[![License](https://img.shields.io/github/license/fraunhoferfokus/sesame.svg)](https://gitlab.com/jniltinho/deckschrubber/blob/master/LICENSE)

Deckschrubber inspects images of a [Docker Registry](https://docs.docker.com/registry/) and removes those older than a given age.

## Quick Start

```bash
go get gitlab.com/jniltinho/deckschrubber
$GOPATH/bin/deckschrubber
```

## Why this?

We run our own private registry on a server with limited storage and it was only a question of time, until it was full! Although there are similar approaches to do Docker registry house keeping (in Python, Ruby, etc), a native module (using Docker's own packages) was missing. This module has the following advantages:

* **Is binary!**: No runtime, Python, Ruby, etc. is required
* **Uses Docker API**: same packages and modules used to relaze Docker registry are used here

## Arguments

```text
  -day int
        max age in days
  -debug
        run in debug mode
  -dry
        does not actually deletes
  -insecure
        Skip insecure TLS verification
  -latest int
        number of the latest matching images of an repository that won't be deleted (default 1)
  -ls-repos
        Listing Repositories
  -ls-tags
        Listing Docker Image Tags
  -month int
        max age in months
  -ntag string
        non matching tags (allows regexp)
  -password string
        Password for basic authentication
  -registry string
        URL of registry (default "https://index.docker.io")
  -repo string
        matching repositories (allows regexp) (default ".*")
  -repos int
        number of repositories to garbage collect (default 5)
  -tag string
        matching tags (allows regexp) (default ".*")
  -user string
        Username for basic authentication
  -v    shows version and quits
  -year int
        max age in days
```

## Registry preparation

Deckschrubber uses the Docker Registry API. 
Its delete endpoint is disabled by default, you have to enable it with the following entry in the registry configuration file:

```text
delete:
  enabled: true
```

See [the documentation](https://github.com/docker/distribution/blob/master/docs/configuration.md#delete) for details. 

## Examples

* **Remove all images older than 2 months and 2 days**

```bash
$GOPATH/bin/deckschrubber -month 2 -day 2
```

* **Remove all images older than 1 year from `http://myrepo:5000`**

```bash
$GOPATH/bin/deckschrubber -year 1 -registry http://myrepo:5000
```

* **Analyize (but do not remove) images of 30 repositories**

```bash
$GOPATH/bin/deckschrubber -repos 30 -dry
```

* **Remove all images of each repository except the 3 latest ones**

```bash
$GOPATH/bin/deckschrubber -latest 3 
```

* **Remove all images with tags that ends with '-SNAPSHOT'**

```bash
$GOPATH/bin/deckschrubber -tag ^.*-SNAPSHOT$ 
```
